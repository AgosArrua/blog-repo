﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlogChallenge.Models;
using BlogChallenge.Models.ViewModels;

namespace BlogChallenge.Controllers
{
    public class BlogPostsController : Controller
    {
        private BlogContext db = new BlogContext();

        // GET: BlogPosts
        public ActionResult Index()
        {
            var list = new List<BlogPost>();
            list = db.BlogPost.OrderByDescending(p => p.PublishDate).ToList();
            var listVM = new List<BlogPostViewModel>();
            var postVM = new BlogPostViewModel();
            listVM = postVM.ListEntity(list);
            return View(listVM);
        }

        // GET: BlogPosts/Details/5
        public ActionResult Details(int? id)
        {
           
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogPost blogPost = db.BlogPost.Find(id);
            var postVM = new BlogPostViewModel(blogPost);
            if (blogPost == null)
            {
                return HttpNotFound();
            }
            return View(postVM);
        }

        // GET: BlogPosts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BlogPosts/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Content,Image,Category,PublishDate,State")] BlogPostViewModel blogPost)
        {
            blogPost.PublishDate = DateTime.Now;
            HttpPostedFileBase file = Request.Files["ImageData"];
            if (file != null)
            {
                byte[] imageBytes = ConvertToBytes(file);
                blogPost.Image = imageBytes;
            }
            if (ModelState.IsValid)
            {
                db.BlogPost.Add(blogPost.ToEntity());
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(blogPost);
        }

        // GET: BlogPosts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogPost blogPost = db.BlogPost.Find(id);
            var postVM = new BlogPostViewModel(blogPost);
            if (blogPost == null)
            {
                return HttpNotFound();
            }
            return View(postVM);
        }

        // POST: BlogPosts/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Content,Image,PublishDate,Category,State")] BlogPostViewModel blogPost)
        {
            if (Request.Files["ImageData"].ContentLength > 0)
            {
                HttpPostedFileBase file = Request.Files["ImageData"];
                byte[] imageBytes = ConvertToBytes(file);
                blogPost.Image = imageBytes;

            }
            else
            {
                using (var db = new BlogContext())
                {

                    var blogDb = db.BlogPost.Find(blogPost.Id);
                    blogPost.Image = blogDb.Image;
                }


            }

            if (ModelState.IsValid)
            {
                db.Entry(blogPost.ToEntity()).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(blogPost);
        }

        // GET: BlogPosts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogPost blogPost = db.BlogPost.Find(id);
            var postVM = new BlogPostViewModel(blogPost);
            if (blogPost == null)
            {
                return HttpNotFound();
            }
            return View(postVM);
        }

        // POST: BlogPosts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BlogPost blogPost = db.BlogPost.Find(id);
            blogPost.State = false;
            //db.BlogPost.Remove(blogPost);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            if (image != null)
            {
                byte[] imageBytes;
                BinaryReader reader = new BinaryReader(image.InputStream);
                imageBytes = reader.ReadBytes((int)image.ContentLength);
                return imageBytes;
            }
            else
            {
                return null;
            }

        }

        [AllowAnonymous]
        public ActionResult RetrieveImage(int id)
        {
            BlogPost bgI = db.BlogPost.Find(id);

            byte[] cover = bgI.Image;

            if (cover != null)
            {
                return File(cover, "image/jpg");
            }
            else
            {
                return null;
            }
        }

        public ActionResult Search(string title)
        {
            var search = from a in db.BlogPost select a;

            if (!String.IsNullOrEmpty(title))
            {
               search = search.Where(a => a.Title.ToLower().Contains(title));
            }

            var listVM = new List<BlogPostViewModel>();
            var postVM = new BlogPostViewModel();
            listVM = postVM.ListEntity(search.ToList());
            return View(listVM);

        }

    }
}
