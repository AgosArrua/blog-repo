﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogChallenge.Models.ViewModels
{
    public class BlogPostViewModel
    {
        public BlogPostViewModel()
        {

        }

        public BlogPostViewModel(BlogPost post)
        {
            Id = post.Id;
            Title = post.Title;
            Content = post.Content;
            Image = post.Image;
            Category = post.Category;
            PublishDate = post.PublishDate;
            State = post.State;
        }

        public int Id { get; set; }
        [Required]
        [StringLength(200)]
        [Display(Name = "Título")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "Contenido")]
        public string Content { get; set; }
        [Display(Name = "Imagen")]
        public byte[] Image { get; set; }
        [Required]
        [StringLength(100)]
        [Display(Name = "Categoría")]
        public string Category { get; set; }
        [Display(Name = "Fecha de Publicación")]
        public DateTime PublishDate { get; set; }
        [Display(Name = "Público")]
        public bool State { get; set; }

        public BlogPost ToEntity()
        {
            var post = new BlogPost();
            post.Id = Id;
            post.Title = Title;
            post.Content = Content;
            post.Image = Image;
            post.Category = Category;
            post.PublishDate = PublishDate;
            post.State = State;

            return post;
        }

        public List<BlogPostViewModel> ListEntity(List<BlogPost> listBP){
            var listVM = new List<BlogPostViewModel>();
            foreach (var item in listBP)
            {
                var PostVM = new BlogPostViewModel();
                PostVM.Id = item.Id;
                PostVM.Title = item.Title;
                PostVM.Content = item.Content;
                PostVM.Image = item.Image;
                PostVM.Category = item.Category;
                PostVM.PublishDate = item.PublishDate;
                PostVM.State = item.State;
                listVM.Add(PostVM);
            }

            return listVM;
        }

    }
}