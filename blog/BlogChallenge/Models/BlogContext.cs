﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BlogChallenge.Models
{
    public class BlogContext : DbContext
    {
        public BlogContext()
            : base("BlogContext")
        {

        }
        public DbSet<BlogPost> BlogPost { get; set; }

    }
}