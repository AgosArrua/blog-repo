﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogChallenge.Models
{
    public class BlogPost
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public byte[] Image { get; set; }

        public string Category { get; set; }

        public DateTime PublishDate { get; set; }
         public bool State { get; set; }
    }
}